//
//  File.swift
//  Quizzler
//
//  Created by Ahmed adham on 1/18/18.
//  Copyright © 2018 London App Brewery. All rights reserved.
//

import Foundation

class Question {
    
    let questionText : String
    var answer : Bool
    
    init(text : String ,correctAnswer : Bool) {
    
        questionText = text
        answer = correctAnswer
    }
}
